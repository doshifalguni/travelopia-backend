var myService=require('./myservice');
var init = function(app) {

    app.get("/getcourse",function(req,res,next){
        console.log("received request");
        //security check will here
        myService.getCourse(req,req.query,function(err,result){
            res.json({error:err,result});
        })
    })
    app.get("/getdata",function(req,res,next){
        console.log("received request");
        //security check will here
        myService.getData(req,req.query,function(err,result){
            res.json({error:err,result});
        })
    })
    app.post("/savedata",function(req,res,next){
        console.log("received request");
        //security check will here
        myService.saveData(req,req.body,function(err,result){
            res.json({error:err,result});
        })
    })

}
module.exports.init = init;
