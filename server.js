const express = require('express'),
    http=require('http'),
    app=express(),
    server=http.createServer(app),
    port=parseInt(process.env.port,10)||3000;
    var cors = require('cors');

var bodyParser=require('body-parser');
app.start = app.listen = function(){
    return server.listen.apply(server, arguments);
};
app.use(cors());

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true,parameterLimit:50000}));

global.myService=require ('./myservice.js');
global.mysqlService=require('./mysqlservice.js')

require('./myroute.js').init(app);

app.use(function(req, res, next){

    next();
});

app.listen(port);
console.log('Now serving the app at http://localhost:' + port + '/');

process.on('uncaughtException', function (err) {

    // do not comment the console log here
    console.log("UNCAUGHT EXCEPTION" + err);
     try{
         console.dir(err);
     }catch(e){}
     try{
         console.log(err.stack);
     }catch(e){}
    
});
