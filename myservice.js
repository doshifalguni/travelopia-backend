var mysqlService=require('./mysqlservice.js');

var roundOfFloat = function (input, noOfDecimal) {
    try {
        if (!input || input == null || isNaN(input)) {
            input = 0;
        }
        var n = parseFloat(input);
        return parseFloat(n.toFixed(noOfDecimal));
    } catch (err) {
        return 0;
    }
}

var getCourse= async function(req,params,callback){
    var query ="select  c.*,el.level_id,el.level_name,cn.currency,cn.country_name,u.university_name,u.avg_anual_tuition,u.country_id ,s.scholarship_name,s.scholarship_id from Course c inner join EducationLevel el on el.level_id=c.level_id  left join (select u.country_id, u.university_id, u.university_name, sum(annual_tuition)/ count(c.course_id) as avg_anual_tuition from Course c   inner join University u on u.university_id=c.university_id group by u.university_id,u.country_id) as u on u.university_id=c.university_id inner join Country cn on cn.country_id=u.country_id left join Scholarship s on s.university_id=u.university_id and s.level_id=c.level_id where 1=1 ";
    var qparams = [];
    if(params && params.name){
        query += " and ( c.course_name like ? or u.university_name like  ? ) ";
        qparams.push('%'+params.name+'%');
        qparams.push('%'+params.name+'%');
    }
    if(params && params.level){
        query += "  and (el.level_name like ? ) "
        qparams.push('%'+params.level+'%');
    }
    if(params && params.country){
        query += " and cn.country_name like ? ";
        qparams.push('%'+params.country+'%');
    }
    query += " order by course_id,scholarship_id" ;
    try{
      var  r =  await mysqlService.executeQuery(query,qparams);
        //Change the data as required.
        var resultset=[];
        var pcourse='';
        var newobj = {};
        if(r && r.length > 0 && r[0].length > 0){
            r[0].forEach((data)=>{
                if(pcourse != data.course_id){
                    newobj={};
                    newobj.course={id:data.course_id, name:data.course_name,annual_tuition:{currency:data.currency,value:data.annual_tuition},education_level:{id:data.level_id,name:data.level_name}};
                    newobj.university={id:data.university_id,name:data.university_name,avg_annual_tution:{currency:data.currency,value:roundOfFloat(data.avg_anual_tuition,2)},country:{id:data.country_id,name:data.country_name}};
                    newobj.scholarships=[{id:data.scholarship_id,name:data.scholarship_name}];
                    pcourse=data.course_id;
                    resultset.push(newobj);
                }else{
                    newobj.scholarships.push({id:data.scholarship_id,name:data.scholarship_name});
                }
            })
        }
        callback(null,resultset);
    }catch(e){
        callback(e);
    }
}

module.exports.getCourse=getCourse;

var getData= async function(req,params,callback){
    if(req && req.headers){
        var auth_token=req.headers["auth-token"];
        if(auth_token != '9ee16102-eaf4-11ec-9315-b4b686da73c7'){
            callback("Invalid Details");
            return;
        }
    }else{
        callback("Invalid Details");
        return;
    }
    var query="select * from enquiries ";
    var qparams = [];
    query += " order by enquiryid desc limit 24 " ;
    try{
      var  r =  await mysqlService.executeQuery(query,qparams);
        callback(null,r[0]);
    }catch(e){
        callback(e);
    }
}

module.exports.getData=getData;

var saveData= async function(req,params,callback){
    if(req && req.headers){
        var auth_token=req.headers["auth-token"];
        if(auth_token != '9ee16102-eaf4-11ec-9315-b4b686da73c7'){
            callback("Invalid Details");
            return;
        }
    }else{
        callback("Invalid Details");
        return;
    }

    if(!params || !params.emailid){
        callback("Wrong Data!");
        return;
    }
    params.enquiryid=new Date().getTime();
    var query="insert into enquiries (`enquiryid`,`name`,`emailid`,`place`,`budget`,`pax`) VALUES (?,?,?,?,?,?)";
    var qparams = [params.enquiryid,params.name,params.emailid,params.place,params.budget,params.pax];
    try{
      var  r =  await mysqlService.executeQuery(query,qparams);
        callback(null,r[0]);
    }catch(e){
        callback(e);
    }
}

module.exports.saveData=saveData;

