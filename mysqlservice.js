const dbKey={
    DB_HOST : 'localhost',
    DB_PORT : 3306,
    DB_USER : 'root',
    DB_PSWD : 'kavw_01135102',
    DB_NAME : 'travelopiadb'
}
// get the client
const mysql = require('mysql2/promise');


var executeQuery= async function(query,params){
    // create the connection, specify bluebird as Promise
       return  new Promise( async (resolve,reject)=>{
        var connection = await mysql.createConnection({host:dbKey.DB_HOST, user: dbKey.DB_USER, database: dbKey.DB_NAME,password:dbKey.DB_PSWD});
        try{
            const [rows,fields]  = await connection.query(query,params);
            connection.end();
            resolve([rows,fields]);
            return;
        }catch(e){
            connection.end();
            reject(e);
            return;
        }
    })
}
    
module.exports.executeQuery=executeQuery;

